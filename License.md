#End User License Agreement (EULA)
**BIND NEW MEDIA & DESIGN END USER LICENSE AGREEMENT**

This is a CONTRACT between you (either an individual or a single entity) and BIND New Media & Design (referred below as BIND), which covers your use of the software provided with this EULA (Theme for Sharepoint) and related software components. All such software is referred to herein as the "SOFTWARE".

A software license issued to a designated user only by BIND or its authorized agents is required for each use of the SOFTWARE as described under the section LIMITED USAGE GRANTED.

**THIS IS A LICENSE AND NOT AN AGREEMENT FOR SALE. BY USING OR INSTALLING THE
SOFTWARE, OR, WHERE APPLICABLE, CLICKING ON THE ACCEPTANCE BUTTON, LICENSEE
INDICATES THAT LICENSEE HAS READ, UNDERSTOOD AND ACCEPTED ALL THE TERMS OF
THIS AGREEMENT. IF LICENSEE DOES NOT AGREE WITH ALL THE TERMS OF THIS
AGREEMENT, LICENSEE SHOULD NOT ATTEMPT TO DOWNLOAD, INSTALL OR USE THE
SOFTWARE OR CLICK ON THE ACCEPTANCE BUTTON (WHERE APPLICABLE).**

**1. REDISTRIBUTION NOT PERMITTED**

The resale and/or redistribution of this theme are strictly prohibited.

**2. LIMITED USAGE GRANT**

BIND grants Licensee a non-exclusive and non-transferable license to use the SOFTWARE provided that the Licensee complies with all terms and conditions of this Agreement:

**a) BIND TUNING SHAREPOINT THEME LICENSE**

You may install this SOFTWARE on unlimited production websites, belonging to either you or your client, as long as using the originally downloaded theme (with no modifications), or one single modified version of the theme. Multiple modified versions are not permitted. A new purchase has to be made, to use the SOFTWARE in more than one modified version.

You may use the same license on non-production development or staging servers.

_If you are a Sharepoint hosting provider or Sharepoint Saas provider, with multi-tenant architectures, this license is only valid per tenant (one license per final customer)._

**b) FOR ALL LICENSES**

You are required to ensure that the SOFTWARE is not distributed in any form that allows it to be reused by any application other than that with which you distribute it. For example, if you install an ASP.NET control along with a packaged application on a customer's server, that customer is not permitted to use the control independent of your application.

Regardless of the type of license purchased, if the SOFTWARE includes reusable software such as controls, components, plug-ins, stylesheets, etc. you may not use any of these independently of the SOFTWARE.

In no case shall you rent, lease, lend, redistribute nor re-license SOFTWARE or source code to a 3rd party individual or entity, except as outlined above. In no case shall you grant further redistribution rights for SOFTWARE to the end-users of your solution.

**3. INTELLECUAL PROPERTY**
**3.1 IMAGERY**

All imagery used in our themes is royalty-free and are the integral part of our products. Limited-usage License gives you the right to use images only as a part of the website you build using your theme. You can use imagery to develop one project only. Any kind of separate usage or distribution is prohibited.

**3.2 INTELLECTUAL PROPERTY INFRINGEMENT**

In the event that the Software is held by a court of competent jurisdiction or reasonably believed by BIND to constitute an infringement of a third party’s intellectual property rights, BIND may at its option (i) modify the infringing portion of the Software so that it is non-infringing, (ii) procure for Licensee sufficient rights to continue to exercise its rights under this Agreement, or(iii) terminate this Agreement and the license hereunder and refund to Licensee the fees paid by Licensee for such Software. This Section represents the sole right and remedy available to Licensee if the Software infringes the intellectual property rights of a third party.

**4. MODIFICATIONS**

You are authorized to make any necessary modification(s) to our products to fit your purposes. Only one modified version is allowed per purchased license.

**5. UNAUTHORIZED USE**

You may not place any of our products, modified or unmodified, on a diskette, CD, website or any other medium and offer them for redistribution or resale of any kind without prior written consent from our company.

**6. ASSIGNABILITY**

You may not sub-license, assign, or transfer this license to anyone else without prior written consent
from BIND.

**7. OWNERSHIP**

It is hereby understood and agreed that BIND is the owner of all right title and interest to this SOFTWARE, which you are about to purchase a license to use and download. You, as licensee, through your purchase of this license, do not acquire any ownership rights to the SOFTWARE. You may not claim intellectual or exclusive ownership to any of our products, modified or unmodified.

**8. LIMITED WARRANTY**

For a period of 30 days following the Effective Date (the “Warranty Period”), BIND warrants that, upon installation, the Software will perform in all material respects in accordance with its then current specifications. If Licensee provides BIND within the Warranty Period with written notice of a breach of this warranty specifying the failure in reasonable detail, then BIND will use reasonable commercial efforts to repair or replace the affected portion of the Software at BIND’s sole cost and expense. The foregoing is Licensee’s sole remedy for a breach of this warranty.

**9. NO OTHER WARRANTIES**

THE LIMITED WARRANTY SET OUT IN SECTION 7 IS IN LIEU OF ALL OTHER WARRANTIES OR
CONDITIONS, AND BINDTUNING DISCLAIMS AND LICENSEE WAIVES ALL OTHER REPRESENTATIONS, WARRANTIES OR CONDITIONS, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED OR STATUTORY WARRANTIES OR CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY, DURABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR TITLE OR NON-INFRINGEMENT AND THOSE ARISING BY STATUTE OR OTHERWISE IN LAW OR FROM A COURSE OF DEALING OR USAGE OF TRADE. BIND DOES NOT WARRANT THAT THE SOFTWARE IS FREE FROM BUGS, DEFECTS, ERRORS OR OMISSIONS. LICENSEE ASSUMES ALL RESPONSIBILITY FOR THE SELECTION OF THE SOFTWARE TO ACHIEVE LICENSEE’S INTENDED RESULTS AND FOR THE INSTALLATION, USE AND RESULTS OBTAINED FROM THE SOFTWARE.

**10. LIMITATION OF LIABILITY**

LICENSEE AGREES THAT ANY LIABILITY ON THE PART OF BIND FOR ANY BREACH OF ITS OBLIGATIONS UNDER THIS AGREEMENT, INCLUDING ANY BREACH OF A CONDITION OR FUNDAMENTAL TERM OR FUNDAMENTAL BREACH OR BREACHES, OR IN ANY OTHER WAY ARISING OUT OF OR RELATED TO THIS AGREEMENT AND THE SOFTWARE FOR ANY CAUSE OF ACTION WHATSOEVER AND REGARDLESS OF THE FORM OF ACTION (INCLUDING BREACH OF CONTRACT, STRICT LIABILITY, TORT INCLUDING NEGLIGENCE OR ANY OTHER LEGAL OR EQUITABLE THEORY), WILL BE LIMITED TO LICENSEE’S ACTUAL, DIRECT, PROVABLE DAMAGES IN AN AMOUNT NOT TO EXCEED THE TOTAL AMOUNT PAID TO BIND IN RESPECT OF THE SOFTWARE.

**11. CERTAIN DAMAGES EXCLUDED**

LICENSEE AGREES THAT IN NO EVENT WILL BIND BE LIABLE FOR ANY LOSS OR INACCURACY OF DATA OR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL, PUNITIVE, EXEMPLARY, AGGRAVATED OR SPECIAL DAMAGES, OR DAMAGES FOR LOSS OF REVENUE OR PURE ECONOMIC LOSS INCLUDING WITHOUT LIMITATION LOSS OF BUSINESS, LOSS OF PROFITS OR FAILURE TO REALIZE ANTICIPATED SAVINGS, EVEN IF BIND HAS BEEN ADVISED OF THE LIKELIHOOD OF SUCH DAMAGES.

**12. INSTALLATIONS**

This Agreement does not entitle Licensee to receive installation services from BIND technical support, telephone or email assistance, or enhancements or updates to the Software (collectively, “Support”). If Licensee wishes to receive installation services they will need to contact BIND, as it is a separate paid service, which is not included in the package price.

**13. ADVERTISING**

Neither party will use the other party's name or marks or refer to or identify the other party in any advertising or publicity releases or promotional or marketing correspondence to others without such other party's prior written approval.

**14. GOVERNING LAW**

This Agreement will be governed by and construed in accordance with the laws of the Province of Porto and the federal laws of Portugal applicable therein, excluding conflict of laws rules that would apply a different body of law. Except as provided in Section 11, the parties hereby attorn to the exclusive jurisdiction of the courts of the Province of Porto and all courts competent to hear appeals therefrom in the event of any dispute hereunder.

**15. CONFIDENTIAL INFORMATION**

Licensee acknowledges that the SOFTWARE and any information Licensee obtains from BIND that is marked "confidential" (collectively, the "Confidential Information") constitute the valuable property and trade secrets of BIND embodying confidential information. Licensee will use the same level of care as a reasonable and prudent businessperson would to protect and safeguard the confidentiality of such Confidential Information while it is in Licensee’s possession or control. Licensee will not reproduce or disclose to or grant any other party access to the Confidential Information for any purpose, other than as expressly permitted by BIND. If Licensee breaches Licensee’s confidentiality covenants herein, then in addition to any other remedies BIND may be entitled to, BIND will be entitled to obtain from any court of competent jurisdiction injunctive relief, including an interlocutory injunction, to enjoin such breach or any anticipated breach. 

For more details on our products and services please visit [bindtuning.com](https://bindtuning.com)