# Support  #
- [Help Center](#helpcenter) 
- [Standard Support](#standardsupport) 
- [Premium Support](#premiumsupport) 
- [Suggest a feature](#suggestafeature) 

<a name="helpcenter"></a>
## Help Center ##

![support_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/support_1.png) 

Our Help Center is a great resource when you need a quick solution. Go through quick tutorials, articles and talk with the community to get your answer in no time. 


Visit [http://support.bind.pt/](http://support.bind.pt/ "http://support.bind.pt/").


<a name="standardsupport"></a>
## Standard Support ##
Standard Support is available to address the following issues within the first 60 days of purchase:

- Errors during product installation
- Product not working properly, or as described, or as documented within user guide
- General questions, help, advice on questions directly related to the product.

All support requests are closely monitored and are typically addressed within 24 to 48 hours. BindTuning support team is **available Monday to Friday, from 9AM to 7PM, WET (Western European Time**). 

You can submit your ticket [here](http://support.bind.pt/hc/en-us/requests/new "here") or email us to [support@bindtuning.com](mailto:support@bindtuning.com"support@bindtuning.com").

<a name="premiumsupport"></a>
## Premium Support ##
Premium Support is offered for additional requirements beyond what is covered in Standard Support, including:

- Modifications, changes or further customizations to the product
- Themes/Modules installation
- General Consulting / Custom Development of DotNetNuke, SharePoint, Orchard, Kentico or Umbraco Themes
- Standard support beyond 60 days of purchase
- Fixes required outside of the normally scheduled patches and releases.
- Priority support over standard support

For information on the available packages of Premium Support, please email us to [support@bindtuning.com](mailto:support@bindtuning.com"support@bindtuning.com").

<a name="suggestafeature"></a>
## Suggest a feature ##

![support_2](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/support_2.png) 

Have an idea for a new feature or experience at BindTuning.com? Visit My BindTuning idea where you can leave your idea and comment and vote on other ideas. 

Visit [http://bind.uservoice.com/](http://bind.uservoice.com/).