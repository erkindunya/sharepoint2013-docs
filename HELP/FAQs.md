# Frequently Asked Questions  #
- [Licensing](#licensing)
- [Pre-purchase questions](#pre-purchasequestions)
- [Purchasing a theme](#purchasingatheme)
- [Post-purchase questions](#post-purchasequestions)
- [Support](#support)

<a name="licensing"></a>
##Licensing##
**Can I install my themes in multiple sites?**

Yes! You can use your downloaded themes in as many sites as you need. There are no limitations on the number of domains/seats where the themes are installed.

Per our license:
> You may install the theme on unlimited production websites, belonging to either you or your client, as long as using the originally downloaded theme (with no modifications), or one single modified version of the theme. **Multiple modified versions are not permitted.** A new purchase has to be made, to use the theme in more than one modified version.

**Can I use a product in a client's website?**

Yes. You are permitted to use our products for both personal or commercial use. However, once you purchase a license for a customer, you cannot use the same license on another customer.


<a name="pre-purchasequestions"></a>
##Pre-purchase questions

**Can I try out a theme before purchase?**

Sure you can! Unlike all other marketplaces where you have to buy to test, with BindTuning you can actually try before you buy. Check out our [Help Center article](http://support.bind.pt/entries/22356848-How-can-I-request-a-trial-theme) on how you can request a trial theme.


**Looks like trials are not available for my CMS, what are my options?**

Unfortunately we are unable to provide trials for Office 365, WordPress, Drupal and HTML5 themes. As an alternative, we suggest testing our free [TheBootstrapTheme](bindtuning.com/cms/all/theme/thebootstraptheme) so you can get a notion how the entire process works.


**Can I purchase my trial theme?**

Yes! If you already downloaded a trial theme, and now want to convert it into a paid theme, you don't have to go through the customization process all over again. Check out our [Help Center article](http://support.bind.pt/hc/en-us/articles/204447209-I-already-have-a-trial-theme-how-can-I-convert-it-to-purchased-) on how you can purchase your trial theme.


**Which browsers do BindTuning themes support?**

We test and support all of our themes in all major browser. BindTuning themes are tested for IE9+, Chrome, Firefox, Safari and Opera.


**Will I be able to download fix updates?**

Yes you will. All theme updates are free as long as within the same CMS version. You can check all Fix Releases at our [Help Center](http://support.bind.pt/hc/en-us/sections/201001919-Announcements "Help Center").

**Does BindTuning themes support RTL?**

Currently our themes do not support RTL.

<a name="purchasingatheme"></a>
##Purchasing a theme
**How can I purchase a single theme?**

Simple, from the theme gallery select a theme and click the "Buy Now" button to purchase a license.

**What if I need more themes?**

We have bundles available for all platforms, for 3, 5 and 10 themes purchases, which include 30%, 40% and 50% discounts respectively. 

**What if I need more than 10 downloads?**

Then our partnership program is the perfect fit! Find out more about the program [here](http://bindtuning.com/partners).

**Do you offer special discounts for education?**

Yes, we provide a 30% discount for education. Contact us directly at support@bindtuning.com with proof of education status and will provide a promo code for your purchase.

**Do you offer special discounts for non-profits?**

We take our social responsibility very seriously, so non-profits are dear to us. If you are a non-profit and hold 501(c) status, send us proof documentation and we will offer you one theme for any CMS.

<a name="post-purchasequestions"></a>
##Post-purchase questions 

**I just purchased a license, how can I download my theme?**

After purchase you'll have immediate access to request your custom themes. Since all BindTuning themes are customizable, you must first go to the customization tool prior to downloading. For more details read our [Help Center article](http://support.bind.pt/hc/en-us/articles/204447199--I-just-purchased-a-license-for-a-theme-how-can-I-download-it-).


**Do my downloads expire?** 

BindTuning works on a one-time purchase basis, and once downloaded the themes are yours forever. However your license has a 1 year expiration date, so if you do not use your license to download your custom theme within 1 year you will use the license. 


**I already downloaded my theme, will I be able to make changes again?**

Each purchase allows the download of one custom theme. After downloading your paid version you won't be able to customize it further online using the builder, you can always manually edit the files. [CSS STYLE GUIDE]


**How can I request an update for my theme?**

If an update is available, you will see a notice in your theme's page, at your BindTuning account. You will be able to download all future updates for free, within the same CMS version. Read more about updating your theme in our [Help Center article](http://support.bind.pt/entries/20389251-How-do-I-get-an-update-for-my-theme).

**How do I request an invoice?**

Please email us at [billing@bindtuning.com](mailto:billing@bindtuning.com) indicating name, address, country and VATID (for european countries), as well as your BindTuning purchase ID. **Invoices must be requested up to 2 weeks after purchase**, after this period we cannot issue formal invoices.

<a name="support"></a>
##Support

**Is support included after purchase?**

After purchase you are entitled to our standard support service for 2 months. You can submit your ticket [here](http://support.bind.pt/hc/en-us/requests/new) or email us to [support@bindtuning.com](mailto:support@bindtuning.com).

**I modified my theme, do I still receive support?**

It’s exciting to see how our themes are modified to suit project needs, but often customer modifications also create unexpected issues. We’ll gladly try to help with these modifications, but there is a limit to what we can do. In general, if a customization requires 2-3 lines of new code, we’ll be glad to assist within our [standard support](http://support.bind.pt/hc/en-us/articles/204447119-Product-Support-Agreement) service. However, if your question requires more than that amount of code, you'll have to request a [Premium Support Service](http://support.bind.pt/hc/en-us/articles/204447119-Product-Support-Agreement).

**I'm using 3d-party plugins and I'm having issues in my website, do I receive support for that?**

We're not able to support third party plugins, or ensure that these plugins work properly with our themes.

**Do you offer customization support?**

Yes, you can request our customization services, as part of our [Premium Support service](http://support.bind.pt/hc/en-us/articles/204447119-Product-Support-Agreement).

**Can you create a custom theme for my company?**

Yes, we provide custom themes services, as part of our [Premium Support Service](http://support.bind.pt/hc/en-us/articles/204447119-Product-Support-Agreement). 



