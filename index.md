# Welcome to BindTuning documentation for SharePoint 2013 #

Powered by [BindTuning](http://bindtuning.com)
------
This user guider covers all the basics of using a BindTuning theme in SharePoint 2013. From installing the theme to uploading BindTuning's demo content.


The documentation is organized into the following sections:

----
- [Getting Started](./Getting Started.md)

----
INSTALL 

- [Requirements](./INSTALL/Requirements.md)
- [Installation for Farm Solution](./INSTALL/Installation for Farm Solution.md) 
- [Installation for Sandbox Solution](./INSTALL/Installation for Sandbox Solution.md)

----
DESIGN

- [Change the master page](./DESIGN/Change the master page.md)
- [Upload the demo content](./DESIGN/Upload the demo content.md)

---- 
UPGRADE OR REPAIR 

- [Upgrade or Repair for Farm Solution](./UPGRADE OR REPAIR/Upgrade or Repair for Farm Solution.md)
- [Upgrade or Repair for Sandbox Solution](./UPGRADE OR REPAIR/Upgrade or Repair for Sandbox Solution.md)

----
UNINSTALL

- [Uninstall for Farm Solution](./UNINSTALL/Uninstall for Farm Solution.md)
- [Uninstall for Sandbox Solution](./UNINSTALL/Uninstall for Sandbox Solution.md)

----
HELP 

- [FAQs](./HELP/FAQs.md) 
- [Support](./HELP/Support.md)
----
- [License](License.md)


