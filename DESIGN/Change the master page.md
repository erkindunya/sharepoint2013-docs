# Change the master page
- [Activate SharePoint Publishing Features](#activatesharepointpublishingfeatures)
- [Changing the master page](#changingthemasterpage)


BindTuning themes include a variety of responsive master pages that you can use and select for different parts of your site. If you are looking to change the master page you will need to activate SharePoint Publishing Features first.  

**Note:** This process is valid for both Farm Solution and Sandbox Solution.

##Activate SharePoint Publishing Features
In order to change your website master page you need to first activate SharePoint Publishing features.

**Note:** You can only activate SharePoint publishing Features if you're an Admin or if you have permission from your Admin. If you don't have permissions contact your Admin.

1. **Open the settings menu** and **click on site settings**; 
2. Under Site Actions, **click on Manage site features**; 
	
	![spf_installation_11](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_11.png) 

3. **Search for *SharePoint Server Publishing*** and **click on Activate**; 

	![changethemasterpage_2](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_2.png) 

4. SharePoint Publishing features are now activated.
	


##Changing the master page
Now that SharePoint Publishing Features are activated, you can change the master page:

1. **Open the Settings menu** and **click on Site Settings**;  
2. Under *Look and Feel*, **click on *Master page*** .
	
	![changethemasterpage_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_1.png) 
 
3. **Choose the master page** you want to apply. BindTuning master pages have the following terminology: ***yourthemename_nameofthemasterpage***. 

	![changethemasterpage_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_3.png) 

