# Upload the demo content
- [Download the .zip file](#downloadthe.zipfile)
- [Unzip the file](#unzipthefile)
- [Add content to MainContentZone](#addcontenttomaincontentzone)
- [Add content to other zones](#addcontenttootherzones)

**Important:** your BindTuning theme must be installed and applied to the site before uploading the demo content.

BindTuning themes are packed with demo content files, which can be directly imported into your SharePoint website. 

<a name="downloadthe.zipfile"></a>
## Download the .zip file 
Login to [bindtuning.com](http://bindtuning.com) and go to your account. Open *My Themes* tab and select the theme. 

Under documentation you will find the .zip file.

![uploadthedemocontent_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/uploadthedemocontent_1.png) 


<a name="unzipthefile"></a>
## Unzip the file

Beforing uploading the demo content into your website you will need to unzip the file. The file name is ***theme.SP2013.DemoContent.zip***.

![uploadthedemocontent_5](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/uploadthedemocontent_5.png) 

After unzipping the file, **open the demo content file**(.txt) that you want to add and **copy all its content**.



<a name="addcontenttomaincontentzone"></a>
## Add content to the MainContentZone  
If you want to copy the content to the Main Content Zone, make sure to follow these steps:

1. **Log into your website** as admin;
2. **Click Edit**;
3. In *Format Text* tab **click on *Edit Source***;
	
	![uploadthedemocontent_2](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/uploadthedemocontent_2.jpg) 

4. **Copy the below HTML code** from the demo content file and paste it into your page: `<asp:ContentPlaceHolder id="MainContentZone" runat="server">`
5. **Click Save**. Demo content added!

<a name="addcontenttootherzones"></a>
## Add content to other zones 
You can also copy content to other zones in your website.


1. **Open your website in the SharePoint Designer** and **open the *All files* folder**;
2. Open the folder _catalogs;
	
	![uploadthedemocontent_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/uploadthedemocontent_3.jpg) 

3. **Click on masterpage**;
4. **Search by your BindTuning theme** and **open it**;0pço
5. **Right click at the layout applied** to your site and **click *Check out***;
6. **Right click again at the layout** and **click *Edit file in advanced mode***;
	
	![uploadthedemocontent_4](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/uploadthedemocontent_4.jpg) 

7. **Copy the code** from the .txt demo content file and **replace corresponding zones in the layout**.
8. **Click save**. 






