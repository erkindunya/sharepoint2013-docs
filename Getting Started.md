- [Download the theme](#downloadthetheme)
- [Unzip the file](#unzipthefile) 
- [Inside the .zip file](#insidethe.zipfile)


<a name="downloadthetheme"></a>
## Download the theme ###
Ready to get started? First you need to download your theme's zip file from bindtuning.com. Access your theme details page and click on the Download button to download your theme.


![gettingstarted_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/gettingstarted_1.png)


<a name="unzipthefile"></a>
## Unzip the file ###

Once you have the theme's zip file saved on your computer, make sure that the file has not been blocked by Windows. Right-click the file you downloaded and choose "Properties". If it says at the bottom of the properties window: "This file came from another computer and might be blocked to help protect this computer" then make sure to click the "Unblock" button so that all of the binary files will be unzipped later on.

Now you can unzip the file to a location of your choosing.



<a name="insidethe.zipfile"></a>
## Inside the .zip file ###
Lets take a look at what the .zip file contains. Inside your theme package you will find two folders, a “FarmSolution” folder for Farm environments, and a “SandboxSolution” folder, for Sandbox. You will be using one of the two folders, depending on the environment you are working on, to install the theme.

![gettingstarted_2](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/gettingstarted_2.png)

### Farm Solution ###

![gettingstarted_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/gettingstarted_3.png)

- Install.*yourthemename*Package.ps1 
- Install.*yourthemename*MySitesPackage.ps1 
- Setup.exe
- Setup.exe.config


##### res #####
Inside the res folder you can find two .wsp files which will be used for installing master pages, CSS and images.

- *yourthemename*.SP2013.wsp **[for regular sites]**
- *yourthemename*MySites.SP2013.wsp **[for Newsfeed sites]**
- Eula.rtf
- Start.png


----------
### Sandbox Solution 

![gettingstarted_4](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/gettingstarted_4.png)

Opening the Sandbox Solution folder you can find a .wsp file for installing master pages, CSS and images on both SharePoint sites and Newsfeed sites. 


- *yourthemename*.SP2013.wsp **[for regular sites and for Newsfeed sites]**

##### Package.for.Office365.PublicFacing #####

Inside the Package.for.Office365.PublicFacing folder you can find a .wsp file  for installing master pages, CSS and images in a Office 365 public facing site.

- *yourthemename*.SP2013-Public.wsp **[for Office 365 public facing site]**


Now let's get your website rocking! First thing on the list is go through all the [Requirements](./INSTALL/Requirements.md) before installing the theme. 

