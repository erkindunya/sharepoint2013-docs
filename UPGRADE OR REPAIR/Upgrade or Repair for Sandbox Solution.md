#Upgrade or Repair for Sandbox Solution
- [Upgrade](#upgrade)
- [Repair](#repair)
	
**Warning**: **ALWAYS make a backup** of all of your theme assets, including CSS files, master pages, page layouts,... Upgrading or repairing the theme will remove all custom changes you have applied previously.

**Important:** Before upgrading or repairing your theme, clean your browser cache.


<a name="upgrade"></a>
## Upgrade
###Before upgrading 

Before upgrading your theme you will need to request the new version at BindTuning.com. You can find all the instructions you need to upgrade your theme's version [here](http://support.bind.pt/hc/en-us/articles/204447149-How-do-I-request-an-update-for-my-theme-).

![beforeupgradingorrepairing](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/beforeupgradingorrepairing.png) 

###Uninstall the theme

To upgrade your theme version you need to first completely remove the theme from your website. 

You can find the instructions for uninstalling a theme  [here](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/UNINSTALL/Uninstall%20for%20Sandbox%20Solution/). 


###Install the theme again

After completely removing the theme you can move on to installing the new version.

You can find the instructions for installing a theme [here](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/INSTALL/Installation%20for%20Sandbox%20Solution/).
 
<a name="repair"></a>
##Repair 

###Uninstall the theme 

To repair your theme you need to first completely remove the theme from your website. 

You can find the instructions for uninstalling the theme  [here](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/UNINSTALL/Uninstall%20for%20Sandbox%20Solution/). 

###Install the theme again

After completely removing the theme you need to install the theme again.

You can find the instructions for installing a theme [here](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/INSTALL/Installation%20for%20Sandbox%20Solution/).