#Upgrade or Repair for Farm Solution
- [Upgrade](#upgrade)
	- [One-click Upgrade](#one-clickupgrade)
	- [Manual Upgrade](#manualupgrade)
- [Repair](#repair)
	- [One-click Repair](#one-clickrepair) 
	- [Manual Repair](#manualrepair) 


**Warning**: **ALWAYS make a backup** of all of your theme assets, including CSS files, master pages, page layouts,... Upgrading or repairing the theme will remove all custom changes you have applied previously.

**Important:** Before upgrading or repairing your theme, clean your browser cache.

**Important:** Weather you are upgrading or repairing your theme you will need to run the process inside your server or else it will result on the following error.

![spf_installation_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_1.png) 


<a name="upgrade"></a>
## Upgrade
###Before upgrading 
Before upgrading your theme you will need to request the new version at BindTuning.com. You can find all the instructions you need to upgrade your theme's version [here](http://support.bind.pt/hc/en-us/articles/204447149-How-do-I-request-an-update-for-my-theme-).

![beforeupgradingorrepairing](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/beforeupgradingorrepairing.png) 


<a name="one-clickupgrade"></a>
###One-click Upgrade
Need to upgrade your theme? The easiest way to do it is with BindTuning One-click tool.

Follow these steps to have your theme upgraded in no time.

1. Unzip the .zip file 

2. Open the "FarmSolution" folder;

3. Inside the "FarmSolution" folder, **click twice on the Setup.exe** file to open the One-click installer;
	
	![gettingstarted_5](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/gettingstarted_5.png)

4.  **Click "Next"**;

5.  **The installer will now check if the theme can be upgraded on your machine**. Once is done you will see a success message. If any of the requirements fail, click on *Abort*, fix the failed requirements and re-run the Installer;

	![spf_installation_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_3.png) 

6.  **Click "Next"**; 

7.  **Select "Upgrade"** and **click "Next"**; (an Upgrade option will only appear if the setup package has a greater version than the one installed)

8.  **Check if your theme appears on the list** (the theme will only appear if its activated on your site) and **click *Next***;

9.  **The theme will be upgraded**. Once is done you will see a success message. 

10.  Click "Next" to check the log or "Close" to close the installer. 


<a name="manualupgrade"></a>
###Manual Upgrade
**Note:** Also valid for Newsfeed websites.
####Uninstall the theme
To upgrade your theme version you need to first completely remove the theme from your website. 

You can find the instructions for uninstalling a theme  [here](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/UNINSTALL/Uninstall%20for%20Farm%20Solution/).


####Install the theme again
After completely removing the theme you can move on to installing the new version.

You can find the instructions for installing a theme [here](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/INSTALL/Installation%20for%20Farm%20Solution/).



<a name="repair"></a>
##Repair
<a name="one-clickrepair"></a>
###One-click Repair 
Need a quick repair on your theme installation? The easiest way to do it is with BindTuning's One-click tool.

Follow these steps to repair your theme.

1. Inside the FarmSolution folder, **click twice on the Setup.exe** file to open the One-click tool;

	![gettingstarted_5](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/gettingstarted_5.png) 

2.  **Click "Next"**;

3.  **The installer will now check if the theme can be repaired on your machine**. Once is done you will see a success message. If any of the requirements failed, click on *Abort*, fix the failed requirements and re-run the One-click tool;

	![spf_installation_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_3.png) 

4.  **Click "Next"**; 

5.  **Select "Repair"** and **click "Next"**; (a Repair option will only appear if the setup package has the same version of the one installed) 

	![upgradeorrepairforfarmsolution_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/upgradeorrepairforfarmsolution_1.png) 

6.  **Check if your theme appears on the list** (The theme will only appear if you have activated it previously. Check our [Installation for Farm Solution](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/INSTALL/Installation%20for%20Farm%20Solution/) if you haven't yet activated the theme.

7.  **Click "Next"**;

8.  **The theme will be retracted and deployed again into your site**; 

9.  Click "Next" to check the log or "Close" to close the installer. 


####Activating the theme again
After repairing your theme you will need to activate it again. 

1. Open your SharePoint Site;

2. **Open the Settings menu** and **click on "Site Settings"**; 

3. Under "Site Actions", **click on "Manage Site Features"**;

4. **Search for "yourthemename" Theme Package** and **click on "Activate"**.


<a name="manualrepair"></a>
###Manual Repair
**Note:** Also valid for Newsfeed websites.

####Uninstall the theme 
To repair your theme you need to first completely remove the theme from your website. 

You can find the instructions for uninstalling the theme  [here](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/UNINSTALL/Uninstall%20for%20Farm%20Solution/). 

####Install the theme again 
After completely removing the theme you need to install the theme again.

You can find the instructions for installing a theme [here](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/INSTALL/Installation%20for%20Farm%20Solution/).