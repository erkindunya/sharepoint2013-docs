#Before upgrading or repairing

Before upgrading your theme's version you will need to request the new version at BindTuning.com. You can find all the instructions you need to upgrade your theme's version [here](http://support.bind.pt/hc/en-us/articles/204447149-How-do-I-request-an-update-for-my-theme-).

###Warning
Make sure to ALWAYS make a backup of your theme's CSS files. Upgrading or repairing the theme will remove all custom changes you have applied previously.