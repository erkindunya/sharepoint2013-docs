###Browsers
Bindtuning themes should work in all modern browsers:

- Firefox 
- Chrome 
- Safari 
- Opera 
- IE9+ 

###Platform version 
- SharePoint 2013 

###Software
- SharePoint Management Shell **[required for installing/uninstalling themes in Farm Solution]**

You can download SharePoint Management Shell [here](https://www.microsoft.com/en-us/download/details.aspx?id=35588).


![requirements](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/requirements.png) 