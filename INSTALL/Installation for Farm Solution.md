- **Installation** 
      - [One-click Installation](#one-clickinstallation) 
      - [Manual Installation](#manualinstallation)
      - [Installation for a Newsfeed website](#installationforanewsfeedwebsite)
- [Activate the theme](#activatethetheme)


**Important:** Before moving on, make sure you've already unzip your theme's .zip file. 	


**Important:** Always run the installation process inside your server or else it will result in the following error:

![spf_installation_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_1.png) 

        
There are two possible ways to install a BindTuning theme on a Farm environment. You can install it using our One-click Tool, a easy setup tool that will automatically deploy the theme to your website, or you can install the theme manually. 

Before moving on, select the process you want to follow: 

- [One-click Installation](#one-clickinstallation) 
- [Manual Installation](#manualinstallation)

---

<a name="one-clickinstallation"></a>
## One-click Installation 
This is the easiest way to get your BindTuning theme up and running. You can automatically deploy a theme to your website without having to spend time setting up everything.

### # Installing the theme
Lets get right to it. Inside your theme's package you will find the "Farm Solution" folder. Open it and **click twice on the Setup.exe** file to open the One-click installer.

When the One-click tool appears click "Next". **The installer will now check if the theme can be installed** in your website. Once is done you will see a success message. If any of the requirements fail, click on "Abort", fix the failed requirements and re-run the One-Click Installer;    

Next, you will see our licensing aggreement. Go through all the details on the license and when you're done check the box where it says "Accept the licensing terms and conditions" and click "Next". 

Your theme is now being installed! Once is done you will see a success message letting you know everything went alright. You can click "Next" to check the log or "Close" to close the One-click Installer.


### # Check if the theme was installed
Just to check everything really went smoothly lets confirm if the theme was successfully installed. First **access your SharePoint Central Administration**, **click on *system settings*** and **open Manage Farm Solutions**. You can also access it using this path: [http://[central_administration_url]/_admin/Solutions.aspx](http://[central_administration_url]/_admin/Solutions.aspx) 

If the theme was successfully installed, the **Status** tab should have *Deployed* and **Deployed to** tab should have *Globally Deployed*. 

![spf_installation_10](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_10.png) 

Theme installed! Now the only left to do is activate the theme. Lets get right to it: [Activate the theme](#activatethetheme)


----

<a name="manualinstallation"></a>
## Manual Installation   
Doing a full manual installation? You've come to the right place. 

Before installing the theme we will do an easy Server Set Up just to make sure your server is ready to process Powershell scripts. After that the only thing left to do is install your theme. 

### # Setting up the server

Since we will be running PowerShell scripts to install the theme you will need to set your Execution Policy should be set from Restricted to Unrestricted\bypass. 

Your Execution Policy is part of the security strategy of Windows PowerShell. It determines whether you can load configuration files (including your Windows PowerShell profile) and run scripts, and it determines which scripts, if any, must be digitally signed before they will run. For more information, see [about_Execution_Policies](#https://technet.microsoft.com/en-us/library/hh847748.aspx).

If your Excution Policy is already set to Unrestricted, you can move on to [Installing the theme](#installingthetheme).

First **open SharePoint Management Shell as admin**. After opening **enter the command** `Get-ExecutionPolicy`. You should get a RemoteSigned message, just like in this example:

Now **insert the command** `Set-ExecutionPolicy Unrestricted`. After reading the Execution Policy Change, **type Y** and **hit enter**.

That's it. Now that the server is ready to run PowerShell scripts you can move on to installing the theme.


### # Installing the theme

Inside your theme's package and opening the "Farm Solution" folder you will find the script **Install."yourthemename"Package.ps1**. This is the script you will use to install the theme.

![spf_installation_8](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_8.png) 

Open your SharePoint Management Shell as an admin and on the console, go the path where the script is. Its usually **C:/Downloads/"yourthemename".SP2013/FarmSolution**.


Now that you are inside the folder you can just insert the command `.\Install.yourthemenamePackage.ps1`. If the **Execution Policy Change** appears just accept it. Next, different options will appear. Choose Option 1 to install the theme and **hit enter**. 

Your theme is now being installed! Once is done you will see a success message letting you know everything went alright.

### # Check if the theme was installed
Lets check if the theme was successfully installed. **Access SharePoint Central Administration**, **click on *system settings*** and **open Manage Farm Solutions**. You can also access it using the following path: [http://[central_administration_url]/_admin/Solutions.aspx](http://[central_administration_url]/_admin/Solutions.aspx) 

If the theme was successfully installed, the **Status** tab should have *Deployed* and **Deployed to** tab should have *Globally Deployed*. 

![spf_installation_10](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_10.png) 


Theme installed! Now the only left to do is activate the theme. Lets get right to it: [Activate the theme](#activatethetheme)

-----
<a name="installationforanewsfeedwebsite"></a>
## Installation for a Newsfeed website 
Your BindTuning theme can also be installed in a Newsfeed website. If that is your case follow the instructions below to have your theme up and running in your Newsfeed site in no time.

### # Setting up the server

Since we will be running PowerShell scripts to install the theme you will need to set your Execution Policy should be set from Restricted to Unrestricted\bypass. 

Your Execution Policy is part of the security strategy of Windows PowerShell. It determines whether you can load configuration files (including your Windows PowerShell profile) and run scripts, and it determines which scripts, if any, must be digitally signed before they will run. For more information, see [about_Execution_Policies](#https://technet.microsoft.com/en-us/library/hh847748.aspx).

If your Excution Policy is already set to Unrestricted, you can move on to [Installing the theme](#installingthetheme).

First **open SharePoint Management Shell as admin**. After opening **enter the command** `Get-ExecutionPolicy`. You should get a RemoteSigned message, just like in this example:

Now **insert the command** `Set-ExecutionPolicy Unrestricted`. After reading the Execution Policy Change, **type Y** and **hit enter**.

That's it. Now that the server is ready to run PowerShell scripts you can move on to installing the theme.
 

### # Installing the theme
Inside your theme's package and opening the "Farm Solution" folder you will find the script **Install."yourthemename"MySitesPackage.ps1**. This is the script you will use to install the theme.

Open your SharePoint Management Shell as an admin and on the console, go the path where the script is. Its usually **C:/Downloads/"yourthemename".SP2013/FarmSolution**.


Now that you are inside the folder you can just insert the command `.\Install.yourthemenameMySitesPackage.ps1`. If the **Execution Policy Change** appears just accept it. Next, different options will appear. Choose Option 1 to install the theme and **hit enter**. 

Your theme is now being installed! Once is done you will see a success message letting you know everything went alright.


### Check if the theme was installed

Lets check if the theme was successfully installed. **Access SharePoint Central Administration**, **click on *system settings*** and **open Manage Farm Solutions**. You can also access it using the following path: [http://[central_administration_url]/_admin/Solutions.aspx](http://[central_administration_url]/_admin/Solutions.aspx) 

If the theme was successfully installed, the **Status** tab should have *Deployed* and **Deployed to** tab should have *Globally Deployed*. 

![spf_installation_10](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_10.png) 


Theme installed! Now the only left to do is activate the theme. Lets get right to it: [Activate the theme](#activatethetheme)

---

<a name="activatethetheme"></a>
## Activate the theme  
**If you want to apply the theme to your website** you will need to activate it first. 

Inside SharePoint, open the Settings menu and **click on *Site Settings*.

Under *Site Actions*, **click on *Manage Site Features***. Search on the list for your theme's. Once yo find it, select it and **click on "Activate"**.

---

Installation done! On to the next chapter where we walk you through setting the master page to apply the theme to your website.

**Next** - [Set the Master Page](#./DESIGN/Set the Master Page.md)
