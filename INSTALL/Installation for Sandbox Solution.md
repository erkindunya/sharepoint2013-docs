- [Installing the theme](#installingthetheme)
- [Finishing off](#finishingoff)


**Note:** Sandbox Solution is not available for trials.

**Note:** This installation process is also valid for Newsfeed websites in a Sandbox environment.

**Tip:** Before moving on to the Installation make sure you have already unzip your theme's .zip file. 

<a name="installingthetheme"></a>
##Installing the theme
Inside your theme's *SandboxSolution* folder you will find a .wsp file which you will be using for installing your theme. 

![gettingstarted_4](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/gettingstarted_4.png) 


1. Open your SharePoint Site;
2. **Open the Settings menu** and **click on "Site Settings"**;

	![sps_installation_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/sps_installation_1.png) 
3. Under "Web Designer Galleries", **click on *Solutions**;

	**Note:** If you're not working on your root site this option will not appear.
	
	![sps_installation_2](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/sps_installation_2.png) 
4. **Click on "Upload Solution"**;
5. **Upload the "yourthemename".SP2013.wsp file**. You can find the file by opening your theme's pack and the *SandboxSolution* folder;
	
	![sps_installation_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/sps_installation_3.png) 
6. **Click "OK"**; 
7. **Click on "Activate"** to activate the theme.
	
	![sps_installation_4](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/sps_installation_4.png) 


<a name="finishingoff"></a>
##Finishing off 
Before wrapping up the installation lets check if the theme was successfully installed and activated.

Still inside ***Solutions***, check if your theme appears listed, with an *Activated* status. 

![sps_installation_6](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/sps_installation_6.png) 

Installation done! On to the next chapter where we walk you through setting the master page to apply the theme to your website.

---

**Next** - [Set the Master Page](http://bindtuning-sharepoint-2013-themes.readthedocs.io/en/latest/DESIGN/Change%20the%20master%20page/)



