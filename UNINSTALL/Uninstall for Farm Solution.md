#Uninstall for Farm Solution 
- [One-click Uninstall](#one-clickuninstall) 
- [Manual Uninstall](#manualuninstall) 

There are two possible ways to uninstall a BindTuning theme on Farm Solution:

- One-click Uninstall
- Manual Uninstall

<a name="one-clickuninstall"></a>
##One-click Uninstall
The easiest way to uninstall your BindTuning theme from your SharePoint site is with BindTuning One-click tool.

To uninstall your theme using the one-click tool follow these steps:

1. Inside the FarmSolution folder, **click twice on the Setup.exe** file to open the One-click tool;

	![gettingstarted_5](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/gettingstarted_5.png) 

2. **Click *Next***;
3.  **The installer will now check if the theme can be removed**. Once is done you will see a success message. If any of the requirements fail, click on *Abort*, fix the failed requirements and re-run the Installer;

	![spf_installation_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_3.png) 

4.  **Click *Next***;
5.  **Select *Remove*** and **click *Next***;
	
	![spf_uninstall_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_uninstall_1.png) 

6.  **Check if your theme appears on the list** (the theme will only appear if it is activated on your site) and **click *Next***; 

	![spf_uninstall_2](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_uninstall_2.png) 

7.  **The theme will be removed** and the seattle master page will be applied. Once is done you will see a success message. 

	![spf_uninstall_4](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_uninstall_4.png) 

8.  Click *Next* to check the log or *Close* to close the installer. 

	
**Success**: The theme was successfully removed from your website.



<a name="manualuninstall"></a>
##Manual Uninstall
**Note:** Process also valid for Newsfeed websites.

Go through these steps if you want to do a full manual uninstall of your BindTuning theme. 


###Reset the theme master pages 
If you are using any of the theme master pages you will need to reset them and switch to one of SharePoint's default master pages before moving on to deactivating and uninstalling the theme.

1. On your root site **open the *Settings* menu** and **click on *Site Settings***;
2. Under *Look and Feel*, **click on *Master page***;
	
	![changethemasterpage_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_1.png) 

3. With the option "Specify a master page to be used..." selected, **pick one of SharePoint's default masters (seattle or oslo)** for both the *Site Master Page* and the *System Master Page*; 
4. Also **check the option "Reset all subsites..."** for both the *Site Master Page* and the *System Master Page*; 

	![spf_uninstall_6](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_uninstall_6.png)  

Note: If you are 

5. **Click *OK.***

###Deactivate the theme
If you have activated the theme you will need to deactivate it before uninstalling the theme. 

1. On your root site **open the *Settings* menu** and **click on *Site Settings***;
2. Under *Site Actions*, **click on *Manage site features***;

	![spf_installation_11](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/spf_installation_11.png)  

3. **Search for *yourthemename Theme Package*** and **click on *Deactivate***.

###Uninstall the theme
**Important:** Before uninstalling the theme check if the Execution Policy is set to Unrestricted\Bypass. You can find the instructions on how to set the Execution Policy to Restricted [here](Installation for Farm Solution.md).

Inside the Farm Solution folder you will find the script ***Install.yourthemenamePackage.ps1***. This is the script you will use to remove the theme.


![gettingstarted_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/gettingstarted_3.png)  

Follow the instructions below to finish the uninstall process. 

1. **Open SharePoint Management Shell** and **run it as Admin**;

2. On the console **go to the path where the script is**;

3. Insert the command `.\Install.yourthemenamePackage.ps1` 

4. **Accept the *Execution Policy Change***, if it appears;

6. **Chose option 2** to retract the theme and **hit enter**; 

	**Note:** If you try to access your site during the process you will see a 503 status error code.

7. Insert the command again `.\Install.yourthemenamePackage.ps1` 
8. **Chose option 3** to remove the theme from your site and **hit enter**. 

**Success**: The theme was successfully removed from your website.

**Note:** You can also use the central administration
(http://[central_administration_url]/_admin/Solutions.aspx) to remove your theme from your site. Just click on the theme, retract and the delete it from the Farm.


